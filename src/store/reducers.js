import React, {
  setGlobal,
  addCallback,
  addReducer,
  getDispatch,
  useDispatch
} from "reactn";
import jwt_decode from "jwt-decode";

setGlobal({
  user:
    localStorage.getItem("token") !== "false"
      ? jwt_decode(localStorage.getItem("token"))
      : false,
  isAdmin: false,
  isFreelance: false,
  isClient: false
});

addReducer("setUser", async (global, dispatch, token) => {
  localStorage.setItem("token", token);
  return {
    user: token ? jwt_decode(token) : false,
    token: token ? token : false
  };
});

addReducer("isFreelance", async (global, dispatch, token) => {
  let role = global.user.roles.map(role => {
    if (role === "Role_Freelancer") return true;
    else return false;
  });

  return { isFreelance: role };
});

addReducer("isAdmin", async (global, dispatch, token) => {
  alert("ok");
  let role = global.user.roles.map(role => {
    if (role === "Role_Admin" || role === "Admin") return true;
    else return false;
  });
  return { isAdmin: role };
});

addReducer("isClient", async (global, dispatch, token) => {
  let role = global.user.roles.map(role => {
    if (role === "Role_Client") return true;
    else return false;
  });

  return { isClient: role };
});
