import React, { useState, useGlobal, useDispatch } from "reactn";

import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import SideBar from "../SideBar/SideBar";
import Modal from "@material-ui/core/Modal";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { Link } from "react-router";
import classNames from "classnames";
import WorkIcon from "@material-ui/icons/Work";
import LaptopChromebookIcon from "@material-ui/icons/LaptopChromebook";

import { Login } from "../Login/Login";
import styles from "./Layout.module.scss";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}));

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5"
  }
})(props => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center"
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center"
    }}
    {...props}
  />
));

// const StyledMenuItem = withStyles(theme => ({
//   root: {
//     "&:focus": {
//       backgroundColor: theme.palette.primary.main,
//       "& .MuiListItemIcon-root, & .MuiListItemText-primary": {
//         color: theme.palette.common.white
//       }
//     }
//   }
// }))(MenuItem);

function getModalStyle() {
  return {
    top: "50%",
    left: "50%",
    transform: `translate(-50%, -50%)`
  };
}

const NavBar = ({ classes, sideBar, openSideBar, openModal }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [user] = useGlobal("user");
  const open = Boolean(anchorEl);
  const dispatch = useDispatch();

  // const handleChange = event => {
  //   setAuth(event.target.checked);
  // };

  const handleMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton
          edge="start"
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
          onClick={() => openSideBar(!sideBar)}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          Menu
        </Typography>
        {user ? (
          <div>
            <div>
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
                onClick={handleMenu}
              >
                <AccountCircle />
                <div style={{ margin: "0 5px" }}>
                  {user?.data?.last_name}
                </div>
                <div style={{ marginRight: "5px" }}>
                  {user?.data?.first_name}
                </div>
              </IconButton>
            </div>
            <StyledMenu
              id="menu-appbar"
              elevation={0}
              getContentAnchorEl={null}
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "center"
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "center"
              }}
              open={open}
              onClose={handleClose}
            >
              <MenuItem onClick={handleClose}>
                <Link
                  style={{ textDecoration: "none", color: "inherit" }}
                  to="/profile"
                >
                  Profile
                </Link>
              </MenuItem>
              <MenuItem onClick={handleClose}>
                <Link
                  style={{ textDecoration: "none", color: "inherit" }}
                  to="/my-account"
                >
                  Mon compte
                </Link>
              </MenuItem>
              <MenuItem onClick={() => dispatch.setUser(false)}>
                Déconnexion
              </MenuItem>
            </StyledMenu>
          </div>
        ) : (
          <Button color="inherit" onClick={() => openModal(true)}>
            Se connecter
          </Button>
        )}
      </Toolbar>
    </AppBar>
  );
};

export const Menus = ({ user }) => {
  return (
    <div>
      <div className={styles.item}>
        <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
          <IconButton edge="start" color="inherit" aria-label="work">
            <WorkIcon />
          </IconButton>
          <div>Missions</div>
        </Link>
      </div>
      {user.roles.filter(
        role => role === "ROLE_Freelancer" || role === "ROLE_Entreprise"
      ).length > 0 && (
        <div className={styles.item}>
          <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
            <IconButton edge="start" color="inherit" aria-label="work">
              <WorkIcon />
            </IconButton>
            <div>Mes missions</div>
          </Link>
        </div>
      )}
      <div className={styles.item}>
        <Link
          to="/best-practices"
          style={{ textDecoration: "none", color: "inherit" }}
        >
          <IconButton edge="start" color="inherit" aria-label="book">
            <LaptopChromebookIcon />
          </IconButton>
          <div>Bonne méthodes</div>
        </Link>
      </div>
      {/* <div className={styles.item}>
        <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
          <IconButton edge="start" color="inherit" aria-label="work">
            <WorkIcon />
          </IconButton>
          <div>Missions</div>
        </Link>
      </div>
      <div className={styles.item}>
        <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
          <IconButton edge="start" color="inherit" aria-label="work">
            <WorkIcon />
          </IconButton>
          <div>Missions</div>
        </Link>
      </div> */}
    </div>
  );
};

const Content = ({
  openSideBar,
  openLowMenu,
  sideBar,
  optionalMenu,
  lowMenu,
  content,
  user
}) => {
  return (
    <div
      className={styles.content}
      style={{ display: "flex" }}
      onClick={() => {
        openSideBar(false);
        openLowMenu(false);
      }}
    >
      {sideBar && (
        <SideBar sideBar={sideBar} mainMenu={<Menus user={user} />} />
      )}
      {optionalMenu && !sideBar && (
        <SideBar
          sideBar={sideBar}
          optionalMenu={optionalMenu}
          lowMenu={lowMenu}
          openLowMenu={openLowMenu}
          mainMenu={<Menus user={user} />}
        />
      )}
      {content ? content : <div>Blank page</div>}
    </div>
  );
};

export const Layout = ({ content, optionalMenu }) => {
  const classes = useStyles();
  const [modal, openModal] = React.useState(false);
  const [sideBar, openSideBar] = useState(false);
  const [lowMenu, openLowMenu] = useState(true);
  const [modalStyle] = useState(getModalStyle);
  const [user] = useGlobal("user");

  return (
    <div className={classes.root}>
      <NavBar
        classes={classes}
        sideBar={sideBar}
        openSideBar={openSideBar}
        openModal={openModal}
      />
      <Content
        openSideBar={openSideBar}
        openLowMenu={openLowMenu}
        sideBar={sideBar}
        optionalMenu={optionalMenu}
        lowMenu={lowMenu}
        content={content}
        user={user}
      />
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={modal}
        onClose={() => openModal(false)}
      >
        <div style={modalStyle} className={classes.paper}>
          <h1 id="simple-modal-title">Se connecter</h1>
          <Login />
        </div>
      </Modal>
    </div>
  );
};

export default Layout;
