import React, { useGlobal, useState, useEffect } from "reactn";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import Chip from "@material-ui/core/Chip";

import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
// import styles from "./Home.modules.scss";
import Layout from "../Layout/Layout";
import { Avatar } from "../Profile/Profile";

const useStyles = makeStyles(theme => ({
  container: {
    maxHeight: 440
  },
  root: {
    width: "100%",
    flexGrow: 1,
    padding: "10px",
    height: "fit-content"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  paper: {
    position: "absolute",
    width: 800,
    maxHeight: "800px",
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    overflow: "auto"
  }
}));

const columns = [
  { id: "objet", label: "Name", minWidth: 170 },
  { id: "description", label: "Description", minWidth: 170 },
  {
    id: "datedebut",
    label: "Date début",
    minWidth: 170,
    align: "center",
    format: value => value.toLocaleString()
  },
  {
    id: "datefin",
    label: "Date de fin",
    minWidth: 170,
    align: "center",
    format: value => value.toLocaleString()
  },
  {
    id: "action",
    label: "Action",
    minWidth: 170,
    align: "center",
    format: value => value.toLocaleString()
  }
];

const acceptProject = () => {
  alert("accepted");
};

const Content = ({ props }) => {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [modal, openModal] = useState(false);
  const [modalUser, openModalUser] = useState(false);
  const [modalStyle] = useState(getModalStyle);
  const [user] = useGlobal("user");
  const [projets, setProjets] = useState(0);
  const [users, setUsers] = useState(false);
  const [selected, setSelected] = useState(false);
  const [client, setClient] = useState(false);

  const isAdminOrClient = user?.roles.filter(
    role => role === "ROLE_Admin" || role === "ROLE_Client"
  );

  const showUsers = () => {
    axios
      .get(`https://localhost:8443/users`, {
        headers: { Authorization: `Bearer ${user.token}` }
      })
      .then(res => {
        setUsers(res.data);
        openModalUser(true);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const getProjects = () => {
    axios
      .get(`https://localhost:8443/projets`, {
        headers: { Authorization: `Bearer ${user.token}` }
      })
      .then(res => {
        setProjets(res.data);
      })
      .catch(error => {
        console.log(error);
      });
  };

  useEffect(() => {
    getProjects();
  }, [projets["@id"]]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  let agent = users["hydra:member"]?.map(user => {
    if (
      user.roles.some(
        role => role === "ROLE_Freelancer" || role === "ROLE_Entreprise"
      )
    )
      return user;
    else return false;
  });

  let mission = projets["hydra:member"]?.map(projet => {
    if (projet.etat === "Nouveau") return projet;
    else return false;
  });

  console.log(selected);

  let filteredProjets = mission?.filter(a => a !== false);
  let filteredAgent = agent
    ?.filter(a => a !== false)
    .sort(function(a, b) {
      return a.points - b.points;
    })
    .reverse();

  return (
    <Paper className={classes.root} elevation={3}>
      <div>
        <h1>Missions</h1>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map(column => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredProjets
                ?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={index}
                      onClick={e => {
                        e.preventDefault();
                        e.stopPropagation();
                        isAdminOrClient?.length > 0
                          ? showUsers()
                          : axios
                              .get(`https://localhost:8443${row.client}`, {
                                headers: {
                                  Authorization: `Bearer ${user.token}`
                                }
                              })
                              .then(res => {
                                setClient(res.data);
                                setSelected(row);
                                openModal(true);
                              })
                              .catch(error => {
                                console.log(error);
                              });
                      }}
                    >
                      {columns.map(column => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number" ? (
                              column.format(value)
                            ) : column.id === "action" ? (
                              <Button
                                variant="contained"
                                style={{
                                  backgroundColor:
                                    isAdminOrClient?.length > 0
                                      ? "#3F51B5"
                                      : "green",
                                  color: "white"
                                }}
                                size="large"
                                onClick={e => {
                                  e.preventDefault();
                                  e.stopPropagation();
                                  isAdminOrClient?.length > 0
                                    ? showUsers()
                                    : acceptProject();
                                }}
                              >
                                {isAdminOrClient?.length > 0
                                  ? "Attribuer à un agent"
                                  : "Accepter"}
                              </Button>
                            ) : (
                              value
                            )}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={projets?.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </div>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={modal}
        onClose={() => openModal(false)}
      >
        <div style={modalStyle} className={classes.paper}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between"
            }}
          >
            <h1 id="simple-modal-title">{selected?.objet}</h1>
            <Chip
              style={{
                backgroundColor: "green",
                color: "white",
                fontWeight: "bold",
                marginRight: "10px"
              }}
              label={"NEW"}
              size="small"
            />
          </div>
          <div id="simple-modal-description">
            <div>{selected?.description}</div>
            <div
              style={{ display: "grid", gridTemplateColumns: "1fr 1fr 1fr" }}
            >
              <div>
                <h2>Catégorie</h2>
                <div>{selected?.categorie}</div>
              </div>
              <div>
                <h2>Client</h2>
                <div>
                  {client?.nom} {client?.prenom}
                </div>
              </div>
              <div>
                <h2>Type</h2>
                <div>{selected?.classe}</div>
              </div>
            </div>
          </div>
          <div style={{ marginTop: "25px" }}>
            <div>
              <Button
                variant="contained"
                style={{ backgroundColor: "green", color: "white" }}
                size="large"
              >
                Postuler à cette mission
              </Button>
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={modalUser}
        onClose={() => openModalUser(false)}
      >
        <div style={modalStyle} className={classes.paper}>
          <h1 id="simple-modal-title">Agents disponible</h1>
          <div id="simple-modal-description">
            <div
              style={{ display: "grid", gridTemplateColumns: "1fr 1fr 1fr" }}
            >
              {filteredAgent?.map(agent => (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <div>
                    <Avatar />
                  </div>
                  <div style={{ margin: "20px", textAlign: "center" }}>
                    <div style={{ display: "flex" }}>
                      <div style={{ marginRight: "5px" }}>{agent.nom}</div>
                      <div style={{ marginLeft: "5px" }}>{agent.prenom}</div>
                    </div>
                    <div style={{ margin: "5px" }}>Scores: {agent.points}</div>
                    <Button variant="contained" color="primary" size="large">
                      Choisir
                    </Button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </Modal>
    </Paper>
  );
};

function getModalStyle() {
  return {
    top: "50%",
    left: "50%",
    transform: `translate(-50%, -50%)`
  };
}

export const Home = () => {
  return (
    <div style={{ display: "flex", flexDirection: "row" }}>
      <Layout
        // optionalMenu={
        //   <div>
        //     <div>Optional Menu</div>
        //   </div>
        // }
        content={<Content />}
      ></Layout>
    </div>
  );
};

export default Home;
