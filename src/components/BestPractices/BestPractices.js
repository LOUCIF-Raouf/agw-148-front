import React, { useState } from "reactn";
import Layout from "../Layout/Layout";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1)
    }
  }
}));

const Content = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      
    </div>
  );
};

export const BestPractices = () => {
  return (
    <div style={{ display: "flex", flexDirection: "row" }}>
      <Layout content={<Content />}></Layout>
    </div>
  );
};

export default BestPractices;
