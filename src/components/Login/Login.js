import React, { useGlobal, useDispatch, useState } from "reactn";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "100%"
    }
  }
}));

export const Login = ({ children }) => {
  const classes = useStyles();

  const [user] = useGlobal("user");
  const dispatch = useDispatch();
  const [email, setEmail] = useState(false);
  const [password, setPassword] = useState(false);
  const [error, setError] = useState(false);

  console.log(user);
  const login = (email, password) => {
    axios
      .post(`https://localhost:8443/api/login_check`, {
        username: email,
        password: password
      })
      .then(res => {
        console.log(res);
        dispatch.setUser(res.data.token);
      })
      .catch(error => {
        setError(true);
      });
  };

  if (user !== false) return children;

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
        textAlign: "center"
      }}
    >
      <div
        style={{
          boxShadow: "0px 0px 5px 0px gray",
          padding: " 50px 20px",
          margin: "10px"
        }}
      >
        <h2>Se connecter</h2>
        <form
          className={classes.root}
          onSubmit={e => {
            e.preventDefault();
            login(email, password);
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            error={error}
            id="outlined-email-input"
            label="E-mail"
            type="text"
            autoComplete="current-email"
            variant="outlined"
            onChange={e => setEmail(e.target.value)}
          />
          <TextField
            error={error}
            id="outlined-password-input"
            label="Password"
            type="password"
            autoComplete="current-password"
            variant="outlined"
            onChange={e => setPassword(e.target.value)}
          />
          {error && (
            <p style={{ color: "red" }}>
              L'adresse mail et/ou le mot de passe sont invalides.
            </p>
          )}
          <Button
            variant="contained"
            color="primary"
            size="large"
            type="submit"
          >
            Connexion
          </Button>
        </form>
      </div>
    </div>
  );
};

export default Login;
