import React, { useGlobal, useDispatch, useState } from "reactn";
import Layout from "../Layout/Layout";
import styles from "./Profile.module.scss";
import Chip from "@material-ui/core/Chip";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";

export const Avatar = ({ props }) => (
  <div className={styles.avatar}>
    <img src="https://www.fillmurray.com/150/150" className={styles.pic} />
  </div>
);

const Header = ({ props }) => {
  return (
    <div className={styles.header}>
      <div className={styles.cover}></div>
      <Avatar />
    </div>
  );
};

const Body = ({ user }) => {
  return (
    <div className={styles.content}>
      <div className={styles.info}>
        <div className={styles.user}>
          <div className={styles.name}>
            <div>{user.data.last_name}</div>
            <div>{user.data.first_name}</div>
            <Chip
              style={{
                backgroundColor: user.data.available ? "green" : "orange",
                color: "white",
                fontWeight: "bold"
              }}
              label={user.data.available ? "Disponible" : "En mission"}
              size="small"
            />
          </div>
          <div className={styles.job}>
            <div>Scores: {user?.data.points}</div>
          </div>
        </div>
        <div className={styles.skills}>
          <div
            style={{
              width: "366px",
              maxWidth: "366px",
              margin: "10px",
              fontWeight: "bold"
            }}
          >
            <div style={{ marginBottom: "10px" }}>Badges</div>
            <div style={{ display: "flex", alignItems: "center" }}>
              <div
                style={{
                  width: "100px",
                  height: "100px",
                  backgroundImage: "url('/badge.png')",
                  marginRight: "10px",
                  backgroundSize: "79%",
                  backgroundPosition: "center",
                  backgroundColor: "lightcyan",
                  borderRadius: "50%",
                  backgroundRepeat: "no-repeat"
                }}
              ></div>
              <div
                style={{
                  width: "100px",
                  height: "100px",
                  backgroundImage: "url('/badge.png')",
                  marginRight: "10px",
                  backgroundSize: "79%",
                  backgroundPosition: "center",
                  backgroundColor: "lightcoral",
                  borderRadius: "50%",
                  backgroundRepeat: "no-repeat"
                }}
              ></div>
              <div
                style={{
                  width: "100px",
                  height: "100px",
                  backgroundImage: "url('/badge.png')",
                  marginRight: "10px",
                  backgroundSize: "79%",
                  backgroundPosition: "center",
                  backgroundColor: "lightblue",
                  borderRadius: "50%",
                  backgroundRepeat: "no-repeat"
                }}
              ></div>
              <IconButton edge="start" color="inherit" aria-label="plus">
                <AddIcon />
              </IconButton>
            </div>
          </div>
          <div
            style={{
              width: "366px",
              maxWidth: "366px",
              margin: "10px",
              fontWeight: "bold"
            }}
          >
            <div style={{ marginBottom: "10px" }}>Compétences</div>
            <div style={{ display: "flex", alignItems: "center" }}>
              <Chip
                style={{
                  backgroundColor: "green",
                  color: "white",
                  fontWeight: "bold",
                  marginRight: "10px"
                }}
                label={"Javascript"}
                size="small"
              />
              <Chip
                style={{
                  backgroundColor: "green",
                  color: "white",
                  fontWeight: "bold",
                  marginRight: "10px"
                }}
                label={"Php"}
                size="small"
              />
              <Chip
                style={{
                  backgroundColor: "green",
                  color: "white",
                  fontWeight: "bold",
                  marginRight: "10px"
                }}
                label={"Python"}
                size="small"
              />
              <IconButton edge="start" color="inherit" aria-label="plus">
                <AddIcon />
              </IconButton>
            </div>
          </div>
        </div>
      </div>
      {/* <div style={{ padding: "15px", margin: "10px" }}>
        <div>
          <div>Adresse: {user?.data.adresse}</div>
          <div>Code Postal: {user?.data.cdp}</div>
          <div>Vile: {user?.data.ville}</div>
        </div>
      </div> */}
    </div>
  );
};

const Content = ({ user }) => {
  return (
    <div className={styles.profile}>
      <Header />
      <Body user={user} />
    </div>
  );
};

export const Profile = ({ props }) => {
  const [user] = useGlobal("user");

  return (
    <div style={{ display: "flex", flexDirection: "row" }}>
      <Layout content={<Content user={user} />}></Layout>
    </div>
  );
};

export default Profile;
