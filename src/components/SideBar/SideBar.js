import React, { useState, useEffect } from "react";
import MenuOpenIcon from "@material-ui/icons/MenuOpen";
import IconButton from "@material-ui/core/IconButton";

import classNames from "classnames";
import styles from "./SideBar.module.scss";

export const SideBar = ({
  optionalMenu,
  sideBar,
  lowMenu,
  openLowMenu,
  mainMenu
}) => {
  return (
    <React.Fragment>
      <div className={classNames(styles.sideBar, {})}>
        {optionalMenu ? optionalMenu : mainMenu}
      </div>
      {optionalMenu && (
        <>
          {!lowMenu && (
            <div className={classNames(styles.smallDevice)}>
              <IconButton
                edge="start"
                color="inherit"
                aria-label="menu-open"
                onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();
                  openLowMenu(true);
                }}
              >
                <MenuOpenIcon />
              </IconButton>
            </div>
          )}
          {lowMenu && (
            <div className={styles.absoluteSideBar}>{optionalMenu}</div>
          )}
        </>
      )}
      {sideBar && <div className={styles.absoluteSideBar}>{mainMenu}</div>}
    </React.Fragment>
  );
};

export default SideBar;
