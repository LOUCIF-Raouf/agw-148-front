import React, { useGlobal, useDispatch, useState } from "reactn";
import Layout from "../Layout/Layout";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Radio from "@material-ui/core/Radio";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import Button from "@material-ui/core/Button";
import axios from "axios";
import styles from "./Account.module.scss";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    width: "100%",
    height: "100%",
    "& > *": {
      margin: theme.spacing(1),
      width: "99%",
      height: "98.5%"
    }
  }
}));

// const OptionalMenu = ({ props }) => {
//   return (
//     <div>
//       <div>Paramètres</div>
//       <div>Factures</div>
//     </div>
//   );
// };

const Content = ({ props }) => {
  const classes = useStyles();
  const [user] = useGlobal("user");
  const [value, setValue] = useState(user?.data.gender);
  const [selectedDate, setSelectedDate] = useState(
    new Date("2014-08-18T21:11:54")
  );

  const handleDateChange = date => {
    setSelectedDate(date);
  };

  const handleChange = event => {
    setValue(event.target.value);
  };

  return (
    <div className={classes.root}>
      <Paper elevation={3}>
        <div className={styles.account}>
          <div>
            <h1>Mon compte</h1>
          </div>
          <div>
            <form
              noValidate
              autoComplete="off"
              onSubmit={e => {
                e.preventDefault();
              }}
              className={styles.formAccount}
            >
              <TextField
                id="siret-basic"
                label="SIRET"
                variant="outlined"
                value={user.data.siret}
              />
              <TextField
                id="nom-basic"
                label="Nom"
                variant="outlined"
                value={user.data.last_name}
              />
              <TextField
                id="prenom-basic"
                label="Prenom"
                variant="outlined"
                value={user.data.first_name}
              />
              <TextField
                id="tel-basic"
                label="Téléphone"
                variant="outlined"
                value={user.data.tel}
              />
              <FormLabel component="legend">Gender</FormLabel>
              <RadioGroup
                aria-label="gender"
                name="gender1"
                value={value}
                onChange={handleChange}
                style={{
                  display: "flex",
                  flexDirection: "row",
                  gridColumnStart: "1",
                  gridColumnEnd: "2"
                }}
              >
                <FormControlLabel
                  value="female"
                  control={<Radio />}
                  label="Female"
                />
                <FormControlLabel
                  value="male"
                  control={<Radio />}
                  label="Male"
                />
                <FormControlLabel
                  value="other"
                  control={<Radio />}
                  label="Other"
                />
              </RadioGroup>
              <TextField
                id="adresse-basic"
                label="Adresse"
                variant="outlined"
                value={user.data.adresse}
              />
              <TextField
                id="cdp-basic"
                label="Code Postal"
                variant="outlined"
                value={user.data.cdp}
              />
              <TextField
                id="ville-basic"
                label="Ville"
                variant="outlined"
                value={user.data.ville}
              />
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  margin="normal"
                  id="date-picker-dialog"
                  label="Date de naissance"
                  format="MM/dd/yyyy"
                  value={user.data.birthday.date}
                  onChange={handleDateChange}
                  KeyboardButtonProps={{
                    "aria-label": "change date"
                  }}
                />
              </MuiPickersUtilsProvider>
              <TextField
                id="nationalite-basic"
                label="Nationalité"
                variant="outlined"
                value={user.data.nationalite}
              />
              <Button
                variant="contained"
                color="primary"
                size="large"
                type="submit"
                style={{
                  position: "absolute",
                  bottom: "-50px"
                }}
              >
                Editer
              </Button>
            </form>
          </div>
        </div>
      </Paper>
    </div>
  );
};

export const Account = ({ props }) => {
  return (
    <div style={{ display: "flex", flexDirection: "row" }}>
      <Layout content={<Content />}></Layout>
    </div>
  );
};

export default Account;
