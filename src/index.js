import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "./store";
import * as serviceWorker from "./serviceWorker";

import { router as Router } from "./router";

const appRootElement = document.getElementById("root");

ReactDOM.render(<Router />, appRootElement);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
