import React from "react";
import { browserHistory, Route, Router } from "react-router";

import Home from "./components/Home/Home";
import Login from "./components/Login/Login";
import Profile from "./components/Profile/Profile";
import Account from "./components/Account/Account";
import BestPractices from "./components/BestPractices/BestPractices";

export const router = ({ props }) => {
  return (
    <div>
      <Router history={browserHistory}>
        {/* <Route path="/logout" component={Logout} />
        <Route path="/leave" component={Leave} /> */}
        <Route component={Login}>
          <Route path="/" component={Home} />
          <Route path="/profile" component={Profile} />
          <Route path="/my-account" component={Account} />
          <Route path="/best-practices" component={BestPractices} />
        </Route>
        {/* <Route component={NotFound} path="*" /> */}
      </Router>
    </div>
  );
};
