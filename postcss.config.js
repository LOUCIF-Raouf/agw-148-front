module.exports = {
    mode: "extends" /* (default value) */ || "file",
    plugins: [
      require("autoprefixer")({ grid: true }),
      require("postcss-custom-media")({
        importFrom: [
          {
            customMedia: {
              "--phone": "(max-width: 600px)",
              "--tablet": "(min-width: 601px) and (max-width: 899px)",
              "--device": "(max-width: 899px)",
              "--bigdevice": "(max-width: 1200px)",
              "--smallscreen": "(min-width: 900px) and (max-width: 1500px)",
              "--desktop": "(min-width: 900px)",
              "--widescreen": "(min-width: 1680px)"
            }
          }
        ].concat(process.env.NODE_ENV === "development" ? [] : require("cssnano"))
      })
    ]
  };
  