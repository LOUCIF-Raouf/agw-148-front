const { when, whenDev, whenProd, whenCI, whenTest, ESLINT_MODES, POSTCSS_MODES } = require("@craco/craco");

module.exports = {
  reactScriptsVersion: "react-scripts" /* (default value) */,
  style: {
    postcss: require("./postcss.config.js")
  },
  eslint: {
    enable: true /* (default value) */,
    mode: "extends" /* (default value) */ || "file",
    configure: {
      /* Any eslint configuration options: https://eslint.org/docs/user-guide/configuring */
      extends: "react-app",
      rules: {
        "no-unused-vars": "off",
        "no-console": "off",
        "jsx-a11y/no-access-key": "off"
      }
    }
  },
  babel: {
    plugins: ["@babel/plugin-proposal-optional-chaining", "@babel/plugin-proposal-function-bind"]
  }
};
